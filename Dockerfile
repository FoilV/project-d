FROM php:7.4-fpm-buster

WORKDIR /app

COPY . /app

RUN apt update \
  && apt install -y git unzip nginx libonig-dev libpq-dev \
  && chmod +x /app/start.sh \
  && chown -R www-data:www-data /app

RUN docker-php-ext-install pdo pdo_mysql mbstring exif

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
  && php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
  && php composer-setup.php \
  && php -r "unlink('composer-setup.php');" \
  && php composer.phar install

CMD ["/app/start.sh"]
