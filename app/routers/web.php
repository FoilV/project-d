<?php

use Slim\Http\ServerRequest as Request;
use Slim\Http\Response as Response;
use Slim\Routing\RouteCollectorProxy;
use Slim\Handlers\Strategies\RequestHandler;

$app->get('/public', '\app\Controllers\HomeController:index')->add('\app\Helpers\CheckLogin:CheckLoginIndex');

// profile
$app->get("/public/profile/{username}", '\app\Controllers\ProfileController:page');

// app
$app->group('/public', function (RouteCollectorProxy $group){

    // feed
    $group->get("/feed", '\app\Controllers\FeedController:page');

    // post
    $group->post("/post/add", '\app\Controllers\PostController:addPost');
    $group->post("/post/delete", '\app\Controllers\PostController:deletePost');
    $group->get("/post/edit", '\app\Controllers\PostController:editPostPage');
    $group->post("/post/edit", '\app\Controllers\PostController:editPost');
    $group->get("/post/likes", '\app\Controllers\LikeController:listLikesPage');

    // like
    $group->post("/like/add", '\app\Controllers\LikeController:addLike');
    $group->post("/like/delete", '\app\Controllers\LikeController:deleteLike');

    // settings
    $group->get('/settings', '\app\Controllers\SettingsController:page');
    $group->get('/settings/update', '\app\Controllers\SettingsController:pageEdit');
    $group->post('/settings/update', '\app\Controllers\SettingsController:updateSettings');

})->add(\app\Helpers\CheckLogin::class);

// register
$app->get('/public/register', '\app\Controllers\RegisterController:page');
$app->get('/public/register/rules', '\app\Controllers\RegisterController:rules');
$app->post('/public/register', '\app\Controllers\RegisterController:register');

// login
$app->get('/public/login', '\app\Controllers\LoginController:page');
$app->post('/public/login', '\app\Controllers\LoginController:login');
$app->post('/public/login/logout', '\app\Controllers\LoginController:logout');

// file
$app->post('/public/file/upload', '\app\Controllers\FileController:upload');
$app->post('/public/file/upload/photo', '\app\Controllers\FileController:uploadPhoto');

// test
$app->get('/public/test', '\app\Controllers\TestController:page');
//$app->get('/public/test', function (Request $request, Response $response, $args){
//
////    $crypt =  new Encryption();
////    $crypt_text = $crypt->CryptWithStandardKey("Привет, мир!");
////
////    echo "Зашифрованный текст: ".$crypt_text;
////    echo "Расшифрованный текст: ".$crypt->EncryptWithStandardKey($crypt_text);
//    return $response;
//});