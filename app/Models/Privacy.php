<?php

namespace app\Models;

class Privacy{
    private $id;
    private $user;
    private $viewingGuest;

    public function setID($id){
        $this->id = $id;
    }

    public function getID(){
        return $this->id;
    }

    public function setUser($user){
        $this->user = $user;
    }

    public function getUser(){
        return $this->user;
    }

    public function setViewingGuest(bool $viewingGuest){
        if ($viewingGuest) {
            $this->viewingGuest = 1;
        } else{
            $this->viewingGuest = 0;
        }
    }

    public function getViewingGuest(){
        return $this->viewingGuest;
    }
}