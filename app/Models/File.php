<?php

namespace app\Models;

// TODO добавить дату загрузки
class File{
    private $id;
    private $name;

    function __construct($fileID, $fileName){
        $this->id = $fileID;
        $this->name = $fileName;
    }

    public function getFileID(){
        return $this->id;
    }

    public function getFileName(){
        return $this->name;
    }
}