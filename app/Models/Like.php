<?php

namespace app\Models;

class Like{
    private $id;
    private $postID;
    private $userID;

    public function getID(){
        return $this->id;
    }

    public function setID($id){
        $this->id = $id;
    }

    public function getPostID(){
        return $this->postID;
    }

    public function setPostID($postID){
        $this->postID = $postID;
    }

    public function getUserID(){
        return $this->userID;
    }

    public function setUserID($userID){
        $this->userID = $userID;
    }
}