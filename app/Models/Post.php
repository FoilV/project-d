<?php

namespace app\Models;

class Post{
    private $id;
    private $text;
    private $img;
    private $timestamp;
    private $author;



    public function setID($id){
        $this->id = $id;
    }

    public function getID(){
        return $this->id;
    }

    public function setText($text){
        $this->text = $text;
    }

    public function getText(){
        return $this->text;
    }

    public function setImg($fileID){
        $this->img = $fileID;
    }

    public function getImg(){
        return $this->img;
    }

    public function setTimestamp($timestamp){
        $this->timestamp = $timestamp;
    }

    public function getTimestamp(){
        return $this->timestamp;
    }

    public function setAuthor($author){
        $this->author = $author;
    }

    public function getAuthor(){
        return $this->author;
    }
}