<?php

namespace app\Models;

use app\Handlers\DB;

class User {

    protected $id;
    private $name;
    private $email;
    private $username;
    private $encryptPassword;
    private $urlAvatar;

    public function __construct($id){
        $this->id = $id;
    }

    public function fillFields($name, $username, $email, $encryptPassword){
        $this->name = $name;
        $this->username = $username;
        $this->email = $email;
        $this->encryptPassword = $encryptPassword;
    }

    public function getID(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getEmail(){
        return $this->email;
    }

    public function setEmail($email){
        $this->email = $email;
    }

    public function getUsername(){
        return $this->username;
    }

    public function setUsername($username){
        $this->username = $username;
    }

    public function setEncryptPassword($password){
        $this->encryptPassword = $password;
    }

    public function getEncryptPassword(){
        return $this->encryptPassword;
    }

    public function setAvatarURL($avatarURL){
        $this->urlAvatar = $avatarURL;
    }

    public function getAvatarURL(){
        return $this->urlAvatar;
    }
}