<?php

use Slim\Factory\AppFactory;

require __DIR__ . '/../../vendor/autoload.php';

session_start();

$app = AppFactory::create();


$app->getContainer();
$errorMiddleware = $app->addErrorMiddleware(true, true, true);

// Define Custom Error Handler
$customError404Handler = function (
    Psr\Http\Message\ServerRequestInterface $request,
    \Throwable $exception,
    bool $displayErrorDetails,
    bool $logErrors,
    bool $logErrorDetails
) use ($app) {
    $response = $app->getResponseFactory()->createResponse();
    $response = $response->withHeader('Location', "/public");
    $response = $response->withStatus(302);

    return $response;
};

$errorMiddleware->setErrorHandler(Slim\Exception\HttpNotFoundException::class, $customError404Handler);
