<?php

namespace app\Controllers;

use app\Handlers\DB;
use app\Models\Like;
use Slim\Http\Response as Response;
use Slim\Http\ServerRequest as Request;

class LikeController extends BaseController{

    public function addLike(Request $request, Response $response, $args){

        // Check correct

        if ((!isset($_POST['post_id']) OR $_POST['post_id'] == '')){
            return $response->withStatus(400);
        }

        $db = DB::getDB();

        // get Post
        $post = $db->getPostByID($_POST['post_id']);

        if ($post->getID() == 0){
            return $response->withStatus(400);
        }

        $like = new Like();
        $like->setPostID($_POST['post_id']);
        $like->setUserID($_SESSION['user_id']);

        $db->addLike($like);

        return $response->withRedirect($request->getHeader("Referer")[0]);
    }

    public function deleteLike(Request $request, Response $response, $args){

        // Check correct

        if ((!isset($_POST['post_id']) OR $_POST['post_id'] == '')){
            return $response->withStatus(400);
        }

        $db = DB::getDB();

        // get Post
        $post = $db->getPostByID($_POST['post_id']);

        if ($post->getID() == 0){
            return $response->withStatus(400);
        }

        $db->deleteLike($post->getID(), $_SESSION['user_id']);

        return $response->withRedirect($request->getHeader("Referer")[0]);
    }

    public function listLikesPage(Request $request, Response $response, $args){
        // Check correct

        if ((!isset($_GET['post_id']) OR $_GET['post_id'] == '')){
            return $response->withRedirect($request->getHeader("Referer")[0]);
        }

        $db = DB::getDB();
        $usersID = $db->getAllUserIDLikePost($_GET['post_id']);
        $users = array();

        foreach ($usersID as $userID){
            $userModel = $db->getUserByID($userID['user_id']);
            $user = array();
            $user['avatar'] = $userModel->getAvatarURL();
            $user['name'] = $userModel->getName();
            $user['username'] = $userModel->getUsername();
            array_push($users, $user);
        }

        $this->render($response, "post_likes.twig", ["users" => $users]);

        return $response;
    }

}