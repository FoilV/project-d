<?php

namespace app\Controllers;

use Psr\Http\Message\ResponseInterface;
use Slim\Views\Twig;
use Twig\Loader\FilesystemLoader;
use Slim\Http\Response as Response;

class BaseController {
    protected $c;
    public $dataRender = array();

    function __construct(){
        $fsloader = new FilesystemLoader('../resources/views');
        $twig = new Twig($fsloader);
        $twig->addExtension(new \Twig\Extension\DebugExtension());
        $this->c = $twig;
        $this->dataRender = $_SESSION;
    }

    function render(Response $response, string $template,  array $data = []):ResponseInterface{
        $this->dataRender = array_merge($this->dataRender, $data);
        $this->c->render($response, $template, $this->dataRender);
        return $response;
    }
}