<?php

namespace app\Controllers;

use Slim\Http\Response as Response;
use Slim\Http\ServerRequest as Request;

class FeedController extends BaseController {
    public function page(Request $request, Response $response, $args){
        $this->render($response, 'feed.twig');
        return $response;
    }
}