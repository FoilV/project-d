<?php

namespace app\Controllers;

use app\Handlers\DB;
use Slim\Http\Response as Response;
use Slim\Http\ServerRequest as Request;

class ProfileController extends BaseController{
    public function page(Request $request, Response $response, $args){

        $db = DB::getDB();

        // Get userID by username
        $userID = $db->getUserIDByUsername($args['username']);

        // Check exist profile
        if ($userID == 0){
            $this->render($response, "profile-not-found.twig");
            return $response;
        }

        // Check allow viewing guests
        if (!$_SESSION['login']) {
            $privacy = $db->getPrivacySettings($userID);

            if (!$privacy->getViewingGuest()) {
                $this->render($response, "profile-guests.twig");
                return $response;
            }
        }

        // Get all post user
        // TODO загружать только часть постов
        $postsRaw = $db->getAllPostRawByUserID($userID);

        // Sort reverse order
        $posts = array();
        foreach ($postsRaw as $post){
            array_unshift($posts, $post);
        }

        foreach ($posts as &$post){
            if (isset($post['img']) AND $post['img'] != '') {
                $post['img'] = "/resources/files/".$db->getFileFromDBByID($post['img'])->getFileName();
            }
        }

        // Check like user post and get likes
        foreach ($posts as &$post){
            if ($db->checkLikeUserPost($post['id'], $_SESSION['user_id'])) {
                $post['liked'] = true;
            } else{
                $post['liked'] = false;
            }

            $post['count_likes'] = $db->getCountLikeByPostID($post['id']);
        }

        $userInfo = array();
        $currentUser = false;

        if ($_SESSION['user_id'] == $userID){
            $userInfo['name'] = $_SESSION['user_name'];
            $userInfo['username'] = $_SESSION['user_username'];
            $userInfo['avatar'] = $_SESSION['user_avatar'];

            $currentUser = true;
        } else{
            $user = $db->getUserByID($userID);

            $userInfo['name'] = $user->getName();
            $userInfo['username'] = $user->getUsername();
            $userInfo['avatar'] = $user->getAvatarURL();
        }

        // Send response
        $responseData = array(
            "posts" => $posts,
            "user" => $userInfo,
            "current_user" => $currentUser
        );

        if (isset($_GET['msg']) AND ($_GET['msg'] != '')){

            switch ($_GET['msg']){
                case 'msg-is-null':
                    $responseData["msg"] = "msg/msg-is-null";
                    break;

                case 'is-not-image':
                    $responseData["msg"] = "msg/is-not-image";
                    break;

                case 'success-add-post':
                    $responseData["msg"] = "msg/success-add-post";
                    break;

                case 'post-success-delete':
                    $responseData["msg"] = "msg/success-delete";
                    break;

                case 'success-edit-post':
                    $responseData["msg"] = "msg/success-edit-post";
                    break;

                default:
                    $responseData["msg/none"];
                    break;
            }

        }

        $this->render($response, 'profile.twig', $responseData);

        return $response;
    }
}