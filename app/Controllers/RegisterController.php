<?php

namespace app\Controllers;

use Slim\Http\Response as Response;
use Slim\Http\ServerRequest as Request;
use app\Handlers\DB;
use app\Handlers\Encryption;
use app\Models\User;

class RegisterController extends BaseController {
    public function page(Request $request, Response $response, $args){
        $this->render($response, 'register.twig');

        return $response;
    }

    public function rules(Request $request, Response $response, $args){
        $this->render($response, 'rules.twig');

        return $response;
    }

    public function register(Request $request, Response $response, $args){
        $db = DB::getDB();

        // check exist user email
        if ($db->existUserEmail($_POST['email'])){
            $this->render($response, 'register.twig', ['error'=>"msg/email-exist"]);
            return $response;
        }

        // check exist username
        if ($db->existUsername($_POST['username'])){
            $this->render($response, 'register.twig', ['error'=>"msg/username-exist"]);
            return $response;
        }

        // check accept rules
        if ($_POST['rules'] != 'on') {
            $this->render($response, 'register.twig', ['error'=>'msg/rules']);
            return $response;
        }

        // check correct password
        if ($_POST['password'] != $_POST['password-confirm']) {
            $this->render($response, 'register.twig', ['error'=>'msg/password-not-matched']);
            return $response;
        }

        // Crypt password
        $crypt = new Encryption();
        $user = new User(0);
        $user->fillFields($_POST['name'], $_POST['username'], $_POST['email'], $crypt->CryptWithStandardKey($_POST['password']));

        $db->addUser($user);

        $this->render($response, 'register-success.twig');

        return $response;
    }
}