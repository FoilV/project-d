<?php

namespace app\Controllers;

use app\Handlers\DB;
use app\Helpers\FileHelpers;
use app\Models\Post;
use Slim\Http\Response as Response;
use Slim\Http\ServerRequest as Request;

class PostController extends BaseController {

    public function addPost(Request $request, Response $response, $args){

        if (!$_SESSION['login']){
            return $response->withStatus(401);
        }

        $post = new Post();

        $post->setText($_POST['text']);
        $post->setAuthor($_SESSION["user_id"]);

        $db = DB::getDB();

        if (isset($_POST['text']) AND ($_POST['text'] == '')){
            return $response->withRedirect('/public/profile/'.$_SESSION['user_username'].'?msg=msg-is-null');
        }

        // If add image
        if (isset($_FILES['image']) AND ($_FILES['image']['error'] != 4)){
            if (!FileHelpers::isImageFileFrom_FILES($_FILES, 'image')){
                return $response->withRedirect('/public/profile/'.$_SESSION['user_username'].'?msg=is-not-image');
            } else{
                $fileName = FileHelpers::SaveFileFrom_FILES($_FILES, 'image');
                $fileID = $db->addFileInDB($fileName);
                $post->setImg($fileID);
            }
        }

        $db->addPost($post);

        return $response->withRedirect('/public/profile/'.$_SESSION['user_username'].'?msg=success-add-post');
    }

    public function deletePost(Request $request, Response $response, $args){

        print_r($_SESSION);

        if (!$_SESSION['login']){
            return $response->withStatus(401);
        }

        $db = DB::getDB();

        // Get post

        if (!isset($_POST['post_id']) OR ($_POST['post_id'] == '')){
            return $response->withStatus(400);
        }

        $post = $db->getPostByID($_POST['post_id']);

        // Check allow delete
        if ($post->getAuthor() != $_SESSION['user_id']){
            return $response->withStatus(403);
        }

        $db->deletePostByID($_POST['post_id']);
        return $response->withRedirect('/public/profile/'.$_SESSION['user_username'].'?msg=post-success-delete');
    }

    public function editPostPage(Request $request, Response $response, $args){

        if (!$_SESSION['login']){
            return $response->withRedirect('/public');
        }

        if (!isset($_GET['post_id']) OR $_GET['post_id'] == ''){
            return  $response->withRedirect('/public/profile/'.$_SESSION['user_username']);
        }

        $db = DB::getDB();

        // Get post
        $post = $db->getPostByID($_GET['post_id']);

        if ($post->getAuthor() != $_SESSION['user_id']){
            return $response->withStatus(403);
        }

        $responseData = array();
        $responseData['post_id'] = $_GET['post_id'];
        $responseData['text'] = $post->getText();

        $file = $db->getFileFromDBByID($post->getImg());
        if ($file->getFileName() != "") {
            $responseData['img_url'] = "/resources/files/" . $file->getFileName();
        }

        $this->render($response, "post_edit.twig", $responseData);

        return $response;
    }

    public function editPost(Request $request, Response $response, $args){
        if (!$_SESSION['login']){
            return $response->withRedirect('/public');
        }

        if (!isset($_POST['post_id']) OR $_POST['post_id'] == ''){
            return  $response->withRedirect('/public/profile/'.$_SESSION['user_username']);
        }

        $db = DB::getDB();

        // Get post

        $post = $db->getPostByID($_POST['post_id']);

        // Edit post

        if (isset($_POST['text']) AND $_POST['text'] != ""){
            $post->setText($_POST['text']);
        }

        // If change image
        if (isset($_FILES['image']) AND ($_FILES['image']['error'] != 4) AND (!isset($_POST['delete-img']))){
            if (!FileHelpers::isImageFileFrom_FILES($_FILES, 'image')){
                return $response->withRedirect('/public/profile/'.$_SESSION['user_username'].'?msg=is-not-image');
            } else{
                $fileName = FileHelpers::SaveFileFrom_FILES($_FILES, 'image');
                $fileID = $db->addFileInDB($fileName);
                $post->setImg($fileID);
            }
        }

        // If delete image
        if (isset($_POST['delete-img'])){
            $post->setImg('NULL');
        }

        // Save post
        $db->updatePost($post);

        return $response->withRedirect("/public/profile/".$_SESSION['user_username']."?msg=success-edit-post");
    }
}