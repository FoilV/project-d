<?php

namespace app\Controllers;

use app\Helpers\Helpers;
use Slim\Http\Response as Response;
use Slim\Http\ServerRequest as Request;
use app\Handlers\DB;
use app\Handlers\Encryption;

class LoginController extends BaseController {
    public function page(Request $request, Response $response, $argv){
        $this->render($response, 'login.twig');

        return $response;
    }

    public function login(Request $request, Response $response, $argv){

        $db = DB::getDB();
        $crypt = new Encryption();

        // check correct login and password
        if (!$db->checkUserUsernameAndPassword($_POST['username'], $crypt->CryptWithStandardKey($_POST['password']))){
            $this->render($response, 'login.twig', ['error'=>"msg/login-incorrect"]);
            return $response;
        }

        // get user

        $user = $db->getUserByUsername($_POST['username']);

        $_SESSION['login'] = true;

        Helpers::UpdateSessionsMass($user);

        return $response->withRedirect('/public');
    }

    public function logout(Request $request, Response $response, $argv){
        $_SESSION = array();

        return $response->withRedirect('/public');
    }
}