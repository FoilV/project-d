<?php

namespace app\Controllers;

use Slim\Http\ServerRequest as Request;
use Slim\Http\Response as Response;

require "BaseController.php";

class HomeController extends BaseController {
    public function index(Request $request, Response $response, $args){
        $this->render($response, 'index.twig');
        return $response;
    }
}
