<?php

namespace app\Controllers;

use app\Handlers\DB;
use app\Handlers\Encryption;
use app\Helpers\FileHelpers;
use app\Helpers\Helpers;
use Slim\Http\Response as Response;
use Slim\Http\ServerRequest as Request;

class SettingsController extends BaseController {

    public function page(Request $request, Response $response, $args){
        $db = DB::getDB();

        $viewing_guests = $db->getPrivacySettings($_SESSION['user_id'])->getViewingGuest();

        $this->render($response, 'settings.twig', ["viewing_guests" => $viewing_guests]);

        return $response;
    }

    public function pageEdit(Request $request, Response $response, $args){
        $db = DB::getDB();

        $viewing_guests = $db->getPrivacySettings($_SESSION['user_id'])->getViewingGuest();

        $this->render($response, 'settings_edit.twig', ["viewing_guests" => $viewing_guests]);

        return $response;
    }

    public function updateSettings(Request $request, Response $response, $args){
        // Check password
        if (!isset($_POST['current-password'])){
            $this->render($response, 'settings_edit.twig', ['error'=>'msg/not-found-password']);
            return $response;
        }

        $crypt = new Encryption();
        $encryptionPassword = $crypt->CryptWithStandardKey($_POST['current-password']);

        $db = DB::getDB();
        if (!$db->checkUserUsernameAndPassword($_SESSION['user_username'], $encryptionPassword)){
            $this->render($response, 'settings_edit.twig', ['error'=>'msg/incorrect-password']);
            return $response;
        }

        $user = $db->getUserByID($_SESSION['user_id']);

        // If exist new password check match new-password and confirm-new-password

        if (isset($_POST['new-password']) AND isset($_POST['confirm-new-password']) AND $_POST['new-password'] != ''){
            if ($_POST['new-password'] != $_POST['confirm-new-password']){
                $this->render($response, 'settings_edit.twig', ['error'=>'msg/password-not-matched']);
                return $response;
            } else{
                $user->setEncryptPassword($crypt->CryptWithStandardKey($_POST['new-password']));
                $db->updateUserPassword($user->getID(), $user->getEncryptPassword());
            }
        }

        // If exist new email check and update

        if (isset($_POST['email']) AND ($_POST['email'] != $_SESSION['user_email'])){
            if (!$db->existUserEmail($_POST['email'])){
                $user->setEmail($_POST['email']); // TODO проверка корректности email
            }
        }

        // If exist new name, change name

        if (isset($_POST['name']) AND ($_POST['name'] != $_SESSION['user_name'])){
            $user->setName($_POST['name']);
        }

        // If change avatar
        if (isset($_FILES['avatar']) AND ($_FILES['avatar']['error'] != 4)){
            // TODO реализовать сохранение аваторок в отдельную директорию
            if (!FileHelpers::isImageFileFrom_FILES($_FILES, 'avatar')){
                $this->render($response, 'settings_edit.twig', ['error'=>'msg/is-not-image']);
                return $response;
            } else{
                // TODO реализовать удаление старых аватарок при добавлении новых
                $fileName = FileHelpers::SaveFileFrom_FILES($_FILES, 'avatar');
                $fileID = $db->addFileInDB($fileName);
                $db->updateAvatarUser($_SESSION['user_id'], $fileID);
                $user->setAvatarURL($db->getAvatarUser($user->getID()));
            }
        }

        // Update privacy
        $privacy = $db->getPrivacySettings($user->getID());

        if (isset($_POST['viewing_guests'])) {
            $privacy->setViewingGuest($_POST['viewing_guests']);
        } else{
            $privacy->setViewingGuest(false);
            echo "OK: ".$privacy->getViewingGuest();
        }

        $db->updatePrivacySettings($user->getID(), $privacy);
        $db->updateUser($user);
        Helpers::UpdateSessionsMass($user);

        return $response->withRedirect('/public/settings');
    }

}