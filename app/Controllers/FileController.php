<?php

namespace app\Controllers;

use app\Helpers\FileHelpers;
use Slim\Http\ServerRequest as Request;
use Slim\Http\Response as Response;

class FileController {

    private $fileHelpers;

    function __construct(){
        $this->fileHelpers = new FileHelpers();
    }

    public function upload(Request $request, Response $response, $args){

        if (!isset($_FILES['path'])){
            return $response->withStatus(400);
        }

        $this->fileHelpers->SaveFileFrom_FILES($_FILES);

        return $response;
    }

    public function uploadPhoto(Request $request, Response $response, $args){
        if ($this->fileHelpers->isImageFileFrom_FILES($_FILES)){
            $this->fileHelpers->SaveFileFrom_FILES($_FILES);
            return $response;
        } else{
            return $response->withStatus(400);
        }
    }
}