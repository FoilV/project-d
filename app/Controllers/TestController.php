<?php

namespace app\Controllers;

use app\Handlers\DB;
use Slim\Http\ServerRequest as Request;
use Slim\Http\Response as Response;

class TestController extends BaseController {
    public function page(Request $request, Response $response, $args){
        $db = DB::getDB();
        echo $db->getCountLikeByPostID(4);
//        $this->render($response, 'test.twig');
        return $response;
    }
}