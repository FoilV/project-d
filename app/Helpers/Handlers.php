<?php

namespace app\Helpers;

use app\Models\User;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class CheckLogin{

    public function __invoke(ServerRequestInterface $request, RequestHandlerInterface $handler){
        $response = $handler->handle($request);

        if ($this->checkLogin()){
            return $response;
        } else{
            return $response->withRedirect('/public');
        }
    }

    public static function CheckLogin():bool {
        if (isset($_SESSION['user_id'])){
            return true;
        } else{
            return false;
        }
    }

    public static function CheckLoginIndex(ServerRequestInterface $request, RequestHandlerInterface $handler){
        $response = $handler->handle($request);

        if (self::CheckLogin()){
            return $response->withRedirect('/public/feed');
        } else {
            return $response->withRedirect('/public/login');
        }
    }
}

class Helpers{
    public static function UpdateSessionsMass(User $user){
        print_r($_SESSION);
        $_SESSION['user_id'] = $user->getID();
        $_SESSION['user_name'] = $user->getName();
        $_SESSION['user_username'] = $user->getUsername();
        $_SESSION['user_email'] = $user->getEmail();
        $_SESSION['user_avatar'] = $user->getAvatarURL();
    }
}