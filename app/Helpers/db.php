<?php

namespace app\Handlers;

use app\Models\File;
use app\Models\Like;
use app\Models\Privacy;
use app\Models\Post;
use DateTime;
use PDO;
use app\Models\User;
use phpDocumentor\Reflection\Types\Array_;

class DB {

    private static $db;
    private $connection;

    public function __construct(){
        $db_host = "178.154.195.176";
        $db_user = "twitter";
        $db_pass = "qwerty123456";
        $db_name = "twitter";

        $this->connection = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    }

    public static function getDB() {
        if (self::$db == null) {
            self::$db = new DB();
        }
        return self::$db;
    }

    private function checkExist($sql) : bool{
        $row_count = $this->connection->query($sql)->rowCount();

        if ($row_count == 0){
            return false;
        } else{
            return true;
        }
    }

    // User

    public function addUser(User $user){
        $name = $user->getName();
        $username = $user->getUsername();
        $email = $user->getEmail();
        $password = $user->getEncryptPassword();

        $sql = "INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`) VALUES (NULL, '$name', '$username', '$email', '$password');";

        $this->connection->query($sql);
    }

    public function updateUser(User $user){
        $name = $user->getName();
        $email = $user->getEmail();
        $id = $user->getID();

        $sql = "UPDATE `users` SET `name` = '$name', `email` = '$email' WHERE `users`.`id` = '$id';";

        $this->connection->query($sql);
    }

    public function existUserEmail($email){
        $sql = "SELECT `email` FROM `users` WHERE `email` = '$email';";
        return $this->checkExist($sql);
    }

    public function existUsername($username){
        $sql = "SELECT `username` FROM `users` WHERE `username` = '$username';";
        return $this->checkExist($sql);
    }

    public function getUserByID($id){
        $sql = "SELECT `id`, `name`, `username`, `email` FROM `users` WHERE `id` = $id";

        $result = $this->connection->query($sql)->fetch();

        $user = new User($id);
        $user->setName($result['name']);
        $user->setUsername($result['username']);
        $user->setEmail($result['email']);
        $user->setAvatarURL($this->getAvatarUser($id));
        return $user;
    }

    public function getUserByUsername($username){
        $sql = "SELECT `id`, `name`, `email` FROM `users` WHERE `username` = '$username'";

        $result = $this->connection->query($sql)->fetch();

        $user = new User($result['id']);
        $user->setName($result['name']);
        $user->setUsername($username);
        $user->setEmail($result['email']);
        $user->setAvatarURL($this->getAvatarUser($user->getID()));
        return $user;
    }

    public function checkUserUsernameAndPassword($username, $encryptPassword) {
        $sql = "SELECT `email` FROM `users` WHERE `username` = '$username' AND `password` = '$encryptPassword';";
        return $this->checkExist($sql);
    }

    public function updateUserPassword($userID, $encryptPassword) {
        $sql = "UPDATE `users` SET `password` = '$encryptPassword' WHERE `users`.`id` = '$userID'; ";
        $this->connection->query($sql);
    }

    public function getUserIDByUsername($username) {
        $sql = "SELECT `id` FROM `users` WHERE `username` = '$username';";
        return $this->connection->query($sql)->fetch()['id'];
    }

    // Privacy.php

    public function checkExistPrivacySettingsUser($userID){
        $sql = "SELECT `id` FROM `privacy` WHERE `user` = '$userID';";
        return $this->checkExist($sql);
    }

    public function addNewPrivacySettings($userID, Privacy $privacy){

        if ($this->checkExistPrivacySettingsUser($userID)){
            $this->updatePrivacySettings($userID, $privacy);
            return;
        }

        $sql = "INSERT INTO `privacy` (`user`, `viewing_guests`) VALUES ('$userID', '1');";
        $this->connection->query($sql);
    }

    function updatePrivacySettings($userID, Privacy $privacy){
        if (!$this->checkExistPrivacySettingsUser($userID)){
            $this->addNewPrivacySettings($userID, $privacy);
            return;
        }

        $viewing_guests = $privacy->getViewingGuest();

        $sql = "UPDATE `privacy` SET `viewing_guests` = '$viewing_guests' WHERE `user` = '$userID';";
        echo $sql;
        $this->connection->query($sql);
    }

    function getPrivacySettings($userID):Privacy{

        if (!$this->checkExistPrivacySettingsUser($userID)){
            $privacy = new Privacy();
            $privacy->setUser($userID);
            $privacy->setViewingGuest(true);

            $this->addNewPrivacySettings($userID, $privacy);

            return $privacy;
        }

        $sql = "SELECT `id`, `viewing_guests` FROM `privacy` WHERE `user` = '$userID';";
        $result = $this->connection->query($sql)->fetch();

        $privacy = new Privacy();

        $privacy->setID($result['id']);
        $privacy->setUser($userID);
        $privacy->setViewingGuest($result['viewing_guests']);

        return $privacy;
    }

    // Files

    public function addFileInDB($filename) : string {
        $sql = "INSERT INTO `files` (`id`, `path`) VALUES (NULL, '$filename');";
        $this->connection->query($sql);
        return $this->connection->lastInsertId();
    }

    public function getFileFromDBByID($fileID) : File{
        $sql = "SELECT `path` FROM `files` WHERE `id` = '$fileID';";

        $result = $this->connection->query($sql)->fetch();

        return new File($fileID, $result['path']);
    }

    // Avatar user

    public function checkExistAvatarUser($userID){
        $sql = "SELECT `file_id` FROM `user_avatar` WHERE `user_id` = '$userID';";
        return $this->checkExist($sql);
    }

    public function updateAvatarUser($userID, $fileID){

        if (!$this->checkExistAvatarUser($userID)){
            $this->addAvatarUser($userID, $fileID);
            return;
        }

        $sql = "UPDATE `user_avatar` SET `file_id` = '$fileID' WHERE `user_id` = '$userID';";
        $this->connection->query($sql);
    }

    public function addAvatarUser($userID, $fileID){
        $sql = "INSERT INTO `user_avatar` (`user_id`, `file_id`) VALUES ('$userID', '$fileID');";
        $this->connection->query($sql);
    }

    public function getAvatarUser($userID) : string {
        if (!$this->checkExistAvatarUser($userID)){
            return "/resources/img/default-avatar.png";
        }

        $sql = "SELECT `file_id` FROM `user_avatar` WHERE `user_id` = '$userID';";
        $result = $this->connection->query($sql)->fetch();

        $file = $this->getFileFromDBByID($result['file_id']);
        return "/resources/files/".$file->getFileName();
    }

    // Post

    public function addPost(Post $post){
        $text = $post->getText();
        $img = $post->getImg();
        $author = $post->getAuthor();

        if ($img == ''){
            $img = 'NULL';
        }

        $date = new DateTime();
        $timestamp = $date->format('Y-m-d H:i:s');

        $sql = "INSERT INTO `posts` (`text`, `img`, `timestamp`, `author`) VALUES ('$text', $img, '$timestamp', '$author');";
        echo $sql;
        $this->connection->query($sql);
    }

    public function getPostByID(int $postID) : Post{
        $sql = "SELECT `text`, `img`, `timestamp`, `author` FROM `posts` WHERE `id` = '$postID';";
        $result = $this->connection->query($sql)->fetch();

        $post = new Post();
        $post->setID($postID);
        $post->setText($result['text']);
        $post->setImg($result['img']);
        $post->setAuthor($result['author']);

        return  $post;
    }

    public function getAllPostRawByUserID(int $userID){
        $sql = "SELECT `id`, `text`, `img`, `timestamp`, `author` FROM `posts` WHERE `author` = '$userID';";
        return $this->connection->query($sql)->fetchAll();
    }

    public function getAllPostByUserID(int $userID) {
        $postsRaw = $this->getAllPostRawByUserID($userID);

        $posts = array();

        foreach ($postsRaw as $postRaw){
            $post = new Post();

            $post->setID($postRaw['id']);
            $post->setAuthor($postRaw['author']);
            $post->setImg($postRaw['img']);
            $post->setText($postRaw['text']);
            $post->setTimestamp($postRaw['timestamp']);

            $posts = array_push($posts, $post);
        }

        return $posts;
    }

    public function deletePostByID(int $postID){
        $sql = "DELETE FROM `posts` WHERE `id` = '$postID';";
        $this->connection->query($sql);
    }

    public function updatePost(Post $post){

        $id = $post->getID();
        $text = $post->getText();
        $img = $post->getImg();

        if ($img == ''){
            $img = 'NULL';
        }

        $sql = "UPDATE `posts` SET `text` = '$text', `img` = $img WHERE `posts`.`id` = $id; ";
        $this->connection->query($sql);
    }

    // TODO добавить функцию, возвращающую последние n постов

    // Like

    public function addLike(Like $like){

        $postID = $like->getPostID();
        $userID = $like->getUserID();

        if ($this->checkLikeUserPost($postID, $userID)){
            return;
        }

        $sql = "INSERT INTO `likes` (`post_id`, `user_id`) VALUES ('$postID', '$userID');";
        $this->connection->query($sql);
    }

    public function getCountLikeByPostID(int $postID){
        $sql = "SELECT COUNT(*) FROM `likes` WHERE `post_id` = '$postID';";
        $result = $this->connection->query($sql)->fetch();

        return $result['COUNT(*)'];
    }

    public function getAllUserIDLikePost(int $postID){
        $sql = "SELECT `user_id` FROM `likes` WHERE `post_id` = '$postID';";
        return $this->connection->query($sql)->fetchAll();
    }

    public function deleteLike(int $postID, int $userID){
        $sql = "DELETE FROM `likes` WHERE `post_id` = '$postID' AND `user_id` = '$userID';";
        $this->connection->query($sql);
    }

    public function checkLikeUserPost(int $postID, int $userID){
        $sql = "SELECT * FROM `likes` WHERE `post_id` = '$postID' AND `user_id` = '$userID'";
        return $this->checkExist($sql);
    }
}