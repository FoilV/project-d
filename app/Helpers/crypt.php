<?php

namespace app\Handlers;

class Encryption{

    private $default_key = 137;
    private $default_library_char = ['a', 'b', 'c', 'h', 'x', 'l', 'w', '1', '7', '3', '8', '9', '1', '2', '4', '6', 'k'];

    private function getSecretCount($key, $count){
        return $key * $count;
    }

    private function normilizeSecretCount($secretCount, $lenLibraryChar){
        $key = $secretCount;
        for (;;){
            if ($key-1 < $lenLibraryChar){
                break;
            }

            $key = $key % 10;
        }

        return $key;
    }

    public function Crypt($key, $library_char, $text){
        $secretText = '';
        $mass_text = str_split($text);
        $count_mass_text = count($mass_text);

        foreach ($mass_text as $value){
            $key = $this->normilizeSecretCount($this->getSecretCount($key, $count_mass_text), count($library_char));
            $count_mass_text = $count_mass_text - 1;
            $secret = ord($value) + ord($library_char[$key]);
            $secretText = $secretText . strval($secret) . ';';
        }

        return $secretText;
    }

    public function CryptWithStandardKey($text){
        return $this->Crypt($this->default_key, $this->default_library_char, $text);
    }

    public function Encrypt($key, $library_char, $text){
        $decrypt_text = "";

        $mass_text = explode(';', $text);
        unset($mass_text[count($mass_text) - 1]);
        $count_mass_text = count($mass_text);

        foreach ($mass_text as $value){
            $key = $this->normilizeSecretCount($this->getSecretCount($key, $count_mass_text), count($library_char));
            $count_mass_text = $count_mass_text - 1;
            $original_number = (int)$value - ord($library_char[$key]);
            $original_symbol = chr($original_number);
            $decrypt_text =  $decrypt_text . $original_symbol;
        }

        return $decrypt_text;
    }

    public function EncryptWithStandardKey($text){
        return $this->Encrypt($this->default_key, $this->default_library_char, $text);
    }
}