<?php

namespace app\Helpers;

class FileHelpers {

    // TODO если несколько файлов
    static public function SaveFileFrom_FILES($FILES, $name){
        $infoFile = pathinfo($FILES[$name]['name']);

        // Rename file
        $defaultPath = "../resources/files/";

        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $filePath = '';
        $fileName = '';

        for (;;) {
            $fileName = $infoFile['filename'] . '-' . substr(str_shuffle($permitted_chars), 0, 7) . '.' . $infoFile['extension'];
            $filePath = $defaultPath . $fileName;
            if (!file_exists($filePath)){
                break;
            }
        }

        move_uploaded_file($_FILES[$name]['tmp_name'], $filePath);
        return $fileName;
    }

    static public function isImageFileFrom_FILES($FILES, $name){
        if (exif_imagetype($FILES[$name]['tmp_name'])){
            return true;
        } else{
            return false;
        }
    }
}