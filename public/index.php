<?php

require "../app/bootstrap/app.php";

require "../app/Controllers/HomeController.php";
require "../app/Controllers/RegisterController.php";
require "../app/Controllers/LoginController.php";
require "../app/Controllers/FeedController.php";
require "../app/Controllers/SettingsController.php";
require "../app/Controllers/ProfileController.php";
require "../app/Controllers/TestController.php";
require "../app/Controllers/FileController.php";
require "../app/Controllers/PostController.php";
require "../app/Controllers/LikeController.php";

require "../app/routers/web.php";

require "../app/Helpers/db.php";
require "../app/Helpers/crypt.php";
require "../app/Helpers/Handlers.php";
require "../app/Helpers/Files.php";

require "../app/Models/user.php";
require "../app/Models/File.php";
require "../app/Models/Post.php";
require "../app/Models/Privacy.php";
require "../app/Models/Like.php";

$app -> run();