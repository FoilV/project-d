-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Апр 24 2020 г., 07:19
-- Версия сервера: 10.4.12-MariaDB
-- Версия PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `twitter`
--

-- --------------------------------------------------------

--
-- Структура таблицы `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `path` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `files`
--

INSERT INTO `files` (`id`, `path`) VALUES
(5, 'jenkins-05-QwhXOyC.png'),
(6, 'f1a038b68fc4382a77ec2fc70852e0ff-8sN1qnv.png'),
(7, 'andrew-ryan-bioshock-hand-jack-tattoos-chains-quotes-wdlZPQq.jpg'),
(8, 'f1a038b68fc4382a77ec2fc70852e0ff-PUMeQtE.png'),
(9, 'jenkins-04-UuTN7qJ.png'),
(10, 'kubenet-byuImFE.png'),
(11, 'f1a038b68fc4382a77ec2fc70852e0ff-PYS3lAD.png'),
(12, 'kube-dns-scheme-AjzGV4N.png'),
(13, 'f1a038b68fc4382a77ec2fc70852e0ff-Pd7lBf4.png'),
(14, 'jenkins-05-Co6TbSd.png'),
(15, 'f1a038b68fc4382a77ec2fc70852e0ff-DGoZt36.png'),
(16, 'f1a038b68fc4382a77ec2fc70852e0ff-nfrpZeT.png'),
(17, 'kube-dns-scheme-Bx1pDmo.png'),
(18, 'f1a038b68fc4382a77ec2fc70852e0ff-c5bfGgt.png'),
(19, '-KvTNFUz.'),
(20, '-XP8Si2x.'),
(21, '-oqdULxM.'),
(22, 'jenkins-create-folder-03-9foCuwH.png'),
(23, 'kubenet-C4FkD67.png'),
(24, 'nodeport-scheme-1kIYe6i.png'),
(25, 'jenkins-create-folder-05-7FDdjtw.png'),
(26, 'jenkins-create-folder-01-dNfvSJD.png'),
(27, 'jenkins-create-folder-05-412LBMX.png'),
(28, 'jenkins-credentials-zCmsPHF.png'),
(29, 'cluster-info-gke-MBOXfKV.png'),
(30, 'jenkins-pipeline-notify-vKfrogV.png');

-- --------------------------------------------------------

--
-- Структура таблицы `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `likes`
--

INSERT INTO `likes` (`id`, `post_id`, `user_id`) VALUES
(19, 17, 27),
(20, 29, 27);

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` int(11) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `author` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `text`, `img`, `timestamp`, `author`) VALUES
(3, 'Привет, мир!', 17, '2020-04-23 18:56:58', 27),
(4, 'Вика, ты классная!', 17, '2020-04-06 19:26:12', 27),
(14, 'что то', 22, '2020-04-23 18:45:35', 27),
(16, 'Вика, Привет!', 23, '2020-04-23 18:55:26', 27),
(17, 'Как дела?', 24, '2020-04-23 18:55:55', 27),
(29, 'Что то делается', 30, '2020-04-24 06:39:26', 27);

-- --------------------------------------------------------

--
-- Структура таблицы `privacy`
--

CREATE TABLE `privacy` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `viewing_guests` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `privacy`
--

INSERT INTO `privacy` (`id`, `user`, `viewing_guests`) VALUES
(1, 27, 1),
(2, 28, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`) VALUES
(27, 'Pavel', 'pavel', 'pavel@sirot.ru', '147;149;150;'),
(28, 'Виктория', 'vikus1', 'vikus@example.com1', '147;149;150;d');

-- --------------------------------------------------------

--
-- Структура таблицы `user_avatar`
--

CREATE TABLE `user_avatar` (
  `user_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `user_avatar`
--

INSERT INTO `user_avatar` (`user_id`, `file_id`) VALUES
(27, 18);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author` (`author`),
  ADD KEY `img` (`img`);

--
-- Индексы таблицы `privacy`
--
ALTER TABLE `privacy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_avatar`
--
ALTER TABLE `user_avatar`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `file_id` (`file_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT для таблицы `privacy`
--
ALTER TABLE `privacy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `likes`
--
ALTER TABLE `likes`
  ADD CONSTRAINT `likes_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `likes_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`author`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `posts_ibfk_2` FOREIGN KEY (`img`) REFERENCES `files` (`id`);

--
-- Ограничения внешнего ключа таблицы `privacy`
--
ALTER TABLE `privacy`
  ADD CONSTRAINT `privacy_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_avatar`
--
ALTER TABLE `user_avatar`
  ADD CONSTRAINT `user_avatar_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_avatar_ibfk_3` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
